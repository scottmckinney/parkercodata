[CmdletBinding(DefaultParametersetName='None')]
param(
  [Parameter(Position=0, 
               Mandatory=$true,
               ValueFromPipeline=$true,
               ValueFromPipelineByPropertyName=$true)]
               [string[]]$SourceFolders,
  [parameter(Mandatory = $false)][string]$FileFilter,
  [parameter(Mandatory = $false)][string]$User,
  [parameter(Mandatory = $false)][string]$Password,
  [parameter(Mandatory = $false)][string]$Hostname,
  [parameter(Mandatory = $false)][string]$Database,
  [parameter(Mandatory = $false)][string]$TableName,
  [parameter(Mandatory = $false)][int]   $Limit,
  [parameter(Mandatory = $false)][switch]$Convert,
  [parameter(Mandatory = $false)][switch]$ShowProgress,
  [parameter(Mandatory = $false)][switch]$Log,
  [parameter(Mandatory = $false)][switch]$Test

)
#Requires -Version 3	
#===============================================================================
#help definitions
<#
.SYNOPSIS
    Converts all Excel files in the specified folder to .txt or .csv and
    uploads the data into a MySQL/MariaDB database.
.DESCRIPTION
    Converts all Excel files in the specified folder to .txt or .csv and
    uploads the data into a MySQL/MariaDB database.
.PARAMETER SourceFolders
    Directory containing Excel files to be uploaded into the database.
.PARAMETER FileFilter
    A wildcard filter. Only files that match the filter expression will be processd.
    Do not include file extensions as they included in the default expresssion.
.PARAMETER User
    MySQL/MariaDB account username.
.PARAMETER Password
    MySQL/MariaDB account password.
.PARAMETER Hostname
    MySQL/MariaDB database server hostname or IP address.
.PARAMETER Database
    MySQL/MariaDB database name.
.PARAMETER TableName
    MySQL/MariaDB database table name.
.PARAMETER Limit
    Maximum number of files to process. Useful for testing.
.PARAMETER Convert
    Convert Excel file(s) to ASCII text format(tab delimited). If omitted, the 
    script assumes the ASCII text files are already present in the folder(s).
.PARAMETER ShowProgress
    Display a progress bar.
.PARAMETER Log
    Log script actions to a file.
.PARAMETER Test
    Process files but no data is written to the database. Verbose messages will be displayed.
.EXAMPLE
    C:\PS> 
    Example description
.NOTES
    Author: Scott McKinney, scott.mckinney@veolia.com
    Date:   2017-03-15
    Requirements: 
      Microsoft Excel installed on script host.
      MySQL/MariaDB .Net Connector installed on script host.
#>

function Get-LogDate {
    return get-date -Format 'yyyy-MM-dd HH:mm:ss'
}

function Write-Log ($logString) {
    if($Log) {
        "{0}: {1}" -f (Get-LogDate),$logString | 
        Out-File -Encoding ascii -Append (Join-Path $SourceFolders[0] 'Send-ToMySQL log.txt')}
}

#===============================================================================
#set default parameters if not passed in from command line
if(-not $FileFilter) {$FileFilter = ''}
if(-not $User) {$User = 'parker'}
if(-not $Password) {$Password= 'parker_user'}
if(-not $Hostname) {$Hostname = '10.152.172.3'}
if(-not $Database) {$Database = 'parker'}
if(-not $TableName) {$TableName = 'ParkerData'}
if($Limit) {$loopContinue = $False} else {$loopContinue = $True}
if($Test)  {$VerbosePreference = 'Continue'} #invoking Test flag will also turn on Verbose mode.

#Excel SaveAs enums here: https://msdn.microsoft.com/en-us/library/office/ff198017.aspx
#set export format(tab-delimited) and necessary variables
$exportExt = '.txt'
$fileMagicNumber = 21

$ErrorActionPreference = 'SilentlyContinue'
$stopWatch = [system.diagnostics.stopwatch]::StartNew() #start Stopwatch object for timings
Write-Log "Startup parameters:"
Write-Log "FileFilter: $FileFilter User: $User Password: $Password Hostname: $Hostname"
Write-Log "Database: $Database TableName: $TableName Limit: $Limit Convert: $Convert"
Write-Log "ShowProgress: $ShowProgress Log: $Log Test: $Test"
#===============================================================================
#function definitions

function Show-ProgressBar {
    param(
        [parameter(Mandatory = $false)][int]$id,
        [parameter(Mandatory = $true)][string]$activityString,
        [parameter(Mandatory = $true)][int]$count,
        [parameter(Mandatory = $true)][int]$totalCount,
        [parameter(Mandatory = $false)][int]$parId
    )
    if($parId) {
        Write-Progress -ParentId $parId -Activity "$activityString"`
                        -PercentComplete (($count/$totalCount)*100)`
                        -Status('{0}/{1}' -f $count,$totalCount)

    } else {
        Write-Progress -Id $id -Activity "$activityString"`
                        -PercentComplete (($count/$totalCount)*100)`
                        -Status('{0}/{1}' -f $count,$totalCount)
    }                       
}

function Export-FromExcel {
    #collect excel process ids before new excel background process is started
    $priorExcelProcesses = Get-Process -name "*Excel*" | ForEach-Object { $_.Id }
    #instance the Excel application object
    $XLObj = New-Object -ComObject Excel.Application
    $XLObj.Visible = $false
    $XLObj.DisplayAlerts = $false
    # collect excel process ids after new excel background process is started
    $postExcelProcesses = Get-Process -name "*Excel*" | ForEach-Object { $_.Id }
    $xlCount = 0 #count of files processed
    #build array of files
    $excelFiles = Get-ChildItem -Recurse -Path $SourceFolders -filter ($FileFilter + '*.xlsx') -file
    foreach( $excelFile in $excelFiles) {
        $xlCount++
        $skipped = $true #used in conjnction with the Limit switch
        if($ShowProgress) {Show-ProgressBar -id 1 -activityString "Processed files: $excelFile" -count $xlCount -totalCount $excelFiles.Length}
        
        #check for file limit count
        if($Limit) {
            $loopContinue = $False
            if($xlCount -le $Limit) {$loopContinue = $True}
        }
        if ((Test-Path -Path $excelFile.fullname -PathType Leaf)  -and ($loopContinue) -and (-not $Test)) {
            #open the Excel file
            $wb = $XLObj.Workbooks.Open($excelFile.FullName)
            $skipped = $false
            #loop through the worksheets
            foreach ($ws in $wb.Worksheets){
                #test if worksheet has content and if worksheet is not hidden (-1=true in VBA)
                #remove the -and clause to export all (visible & hidden) worksheets.
                if($XLObj.WorksheetFunction.CountA($ws.Cells) -gt 0 -and $ws.Visible -eq -1 ){
                #if($XLObj.WorksheetFunction.CountA($ws.Cells) -gt 0) {
                    #construct new export file name by concatenating the worksheet name & extension
                    $newName = $excelFile.BaseName + '_' + $ws.Name + $exportExt
                    $ws.SaveAs((Join-Path -Path $excelFile.DirectoryName -ChildPath $newName), $fileMagicNumber)
                }
            }
            $XLObj.Workbooks.Close() > $null
            Write-Log "Processed $excelFile."
            Write-verbose ("File {0,3}, completed at {1:n2} seconds." -f`
                           $xlCount.ToString().PadLeft(3,'0'),$stopWatch.Elapsed.TotalSeconds)
        }
        
        if($skipped) {
            Write-Log "Skipped $excelFile."
            Write-verbose ("File {0,3}:{1}, skipped   at {2:n2} seconds." -f`
                            $xlCount.ToString().PadLeft(3,'0'),`
                            $excelFile.name, $stopWatch.Elapsed.TotalSeconds)}
    }

    $XLObj.Quit()
    #Release reference count for the XLObj COM object. Refer to link:
    #https://technet.microsoft.com/en-us/library/ff730962.aspx
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($XLObj)
    Remove-Variable XLObj
    # kill all other Excel processes after $priorExcelProcesses list 
    # (all Excel(s) started after the $priorExcelProcesses list is generated).
    $postExcelProcesses | 
        Where-Object { $priorExcelProcesses -eq $null -or $priorExcelProcesses -notcontains $_ } | 
        ForEach-Object { Stop-Process -Id $_ }    
}

function Format-HeaderNames {
    #Formats SQL header names by replacing certain words with abbreviations,
    #including misspellings by the original report authors.
    param(
        [parameter(Mandatory = $true)][string]$headerText
    )
    return $headerText.Trim('"') `
            -replace ' - ', ' ' `
            -replace '-', ' ' `
            -replace ',', '' `
            -replace 'Actiflo', 'ACT' `
            -replace 'Acitflo', 'ACT' `
            -replace 'Backwash', 'BW' `
            -replace 'Combined', 'Comb' `
            -replace 'Effluent', 'Eff' `
            -replace 'Filter', 'FILT' `
            -replace 'Fliter', 'FILT' `
            -replace 'Filtrate Water Pressure', 'FILT Water Press' `
            -replace 'Filtration', 'FILT' `
            -replace 'Finished', 'Finish' `
            -replace 'Initial', 'Init' `
            -replace 'Membrane Wet Well Feed', 'WW' `
            -replace 'Normalized', 'Norm' `
            -replace 'Permeabliity', 'Perm' `
            -replace 'Permeability', 'Perm' `
            -replace 'Pressure', 'Press' `
            -replace 'Settled Water Press', 'SETL Water Press' `
            -replace ' Skid ', 'S' `
            -replace 'Train ', 'T' `
            -replace 'Temperature', 'Temp' `
            -replace 'Turbidity', 'Turb' `
            -replace ' ', '_' `
            -replace 'pH_TA', 'TA_pH' `
            -replace 'pH_TB', 'TB_pH' `
            -replace 'Turb_TA', 'TA_Turb' `
            -replace 'Turb_TB', 'TB_Turb' `
            -replace 'Comb_FILT', 'Comb_Filter'
}

function Get-AverageTimes($timeArray, $baseDate) {
    #returns the average time of an array of timestamps using the date from $baseDate
    Write-Debug 'Get-AverageTimes'
    $dtZero = [DateTime] 0
    $avgSecs = ($timeArray | 
        ForEach-Object {([DateTime] ($baseDate, $_ -join ' ')).subtract([DateTime] 0).TotalSeconds} | 
        Measure-Object -Average).Average
    return $dtZero.AddSeconds($avgSecs)
}

function Get-FieldData {
    param(
      [parameter(Mandatory = $true)][string]$line,
      [parameter(Mandatory = $true)][int]$startIndex,
      [parameter(Mandatory = $true)][string]$delimiter
    )
    Write-Debug 'Get-FieldData'
    #This function returns data values as an array. It expects a single line
    #of delimited data (usually tab- or comma-delimited). 
    # $skipIncrement         iteration counter increment - used to skip over fields.
    # $startIndex            field index to initialize the iteration counter.

    #hard-coded column increment based on the file column structure:
    # Date, Time, Data, Date, Time, Data, etc.
    $skipIncrement = 3  
    $lineData = $line.split($delimiter)
    $dataArray = @()
    # -lt comparison accounts for $i=count-1 for last item
    for ($i = $startIndex; $i -lt $lineData.count; $i += $skipIncrement) {
        if($lineData[$i] -match 'false') {$lineData[$i] = '0'} #regex match, case insensitive
        if($lineData[$i] -match 'true') {$lineData[$i] = '1'}
        $dataArray += $lineData[$i].trim('"')
    }
    return $dataArray
}

function Get-StatusColumns($headerArray) {
    #Returns a hashtable of indices where the SQL header text matches the specified words.
    Write-Debug 'Get-StatusColumns'
    $i=0
    $colNumbers=@{}
    ForEach ($headerText in $headerArray) {
        if ($headerText -match 'Status|Flag') {
            $colNumbers.add($i,'0')
            #$colNumbers.add($headerText,0)
        }
        $i++
    }
    return $colNumbers
}

function Format-MySQLTableString {
    param(
      [parameter(Mandatory = $true)][string[]]$values,
      [parameter(Mandatory = $true)][string]$delimiter
    )
    return $mySQLstr = ('(',($values -join ($delimiter,',',$delimiter -join '')),')' -join $delimiter)
}

function Connect-MySQL{
    param(
      [parameter(Mandatory = $true)][string]$user,
      [parameter(Mandatory = $true)][string]$pass,
      [parameter(Mandatory = $true)][string]$MySQLHost,
      [parameter(Mandatory = $true)][string]$database
    )
  # Load MySQL .NET Connector Objects 
    [void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data") 
 
    # Open Connection 
    $connStr = "server=" + $MySQLHost + ";port=3306;uid=" + $user + ";pwd=" + $pass + ";database="+$database+";Pooling=FALSE" 
    try {
        $conn = New-Object MySql.Data.MySqlClient.MySqlConnection($connStr) 
        $conn.Open()
    } catch [System.Management.Automation.PSArgumentException] {
        #Log "Unable to connect to MySQL server, do you have the MySQL connector installed..?"
        #Log $_
        Write-Error "Unable to connect to MySQL server, do you have the MySQL connector installed..?"
        Write-Error $_
        #Exit
    } catch {
        #Log "Unable to connect to MySQL server..."
        #Log $_.Exception.GetType().FullName
        #Log $_.Exception.Message
        Write-Error "Unable to connect to MySQL server..."
        Write-Error $_.Exception.GetType().FullName
        Write-Error $_.Exception.Message
        #exit
    }
    #Log "Connected to MySQL database $MySQLHost\$database"
 
    return $conn 
}
 
function Disconnect-MySQL($conn) {
  $conn.Close()
}

function Invoke-MySQLNonQuery($conn, [string]$query) { 
  $command = $conn.CreateCommand()                  # Create command object
  $command.CommandText = $query                     # Load query into object
  $RowsInserted = $command.ExecuteNonQuery()        # Execute command
  $command.Dispose()                                # Dispose of command object
  if ($RowsInserted) { 
    return $RowInserted 
  } else { 
    return $false 
  } 
} 

function Invoke-MySQLQuery($connMySQL, [string]$query) { 
  # NonQuery - Insert/Update/Delete query where no return data is required
  $cmd = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $connMySQL)    # Create SQL command
  $dataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($cmd)      # Create data adapter from query command
  $dataSet = New-Object System.Data.DataSet                                    # Create dataset
  $dataAdapter.Fill($dataSet, "data")                                          # Fill dataset from data adapter, with name "data"              
  $cmd.Dispose()
  return $dataSet.Tables["data"]                                               # Returns an array of results
}

function Invoke-MySQLScalar([string]$query) {
    # Scalar - Select etc query where a single value of return data is expected
    $cmd = $SQLconn.CreateCommand()                                             # Create command object
    $cmd.CommandText = $query                                                   # Load query into object
    $cmd.ExecuteScalar()                                                        # Execute command
}

#===============================================================================
#Main body of script.
if($Convert) {Export-FromExcel}

$textFiles = @()
$textFiles = Get-ChildItem -Path $SourceFolders -Recurse -filter ($FileFilter + '*' + $exportExt) -file
#file layout constraints
$delimGlobal = "`t" #global field delimiter
$dateStartCol = 0  #enum for start column of dates
$timeStartCol = 1  #enum for start column of times
$dataStartCol = 2  #enum for start column of values
$SQLendQuote = '"'
$SQLquote = '","'
$fCount = 0 #file counter
#initiate DB connection
$dbconn = Connect-MySQL -user $User -pass $Password -MySQLHost $Hostname -database $Database

[System.Collections.ArrayList]$sqlHeader = @()  #array for SQL header names
#process file list
foreach($textFile in $textFiles) {
    $fCount += 1
    $skipped = $true #used in conjnction with the Limit switch
    if($ShowProgress) {Show-ProgressBar -id 2 -activityString "Processed files: $fcount" -count $fCount -totalCount $textFiles.length}
    #check for file limit count
    if($Limit) {
        $loopContinue = $False
        if($fCount -le $Limit) {$loopContinue = $True}}
    if ((Test-Path -Path $textFile.FullName -PathType Leaf) -and ($loopContinue)) {
        Write-Log "Parsing   $textFile."
        $sqlHeader = ('Log_DateTime','Log_Date','Log_Time')
        $rawContent = @()
        $rawContent += Get-Content $textFile.FullName #array of lines.
        #Write-Log "File lines ($rawContent.length)"
        if($rawContent.Length -gt 1) {
            $skipped = $false
            #array of header text for data values only (no Date or time).
            $rawHeader = Get-FieldData $rawContent[0] $dataStartCol $delimGlobal
            #construct SQL Header.
            $rawHeader | ForEach-Object { $sqlHeader += Format-HeaderNames $_ }
            #hashtable of columns that contain True/False values (key=col #, value=0 or 1).
            $boolColumns = Get-StatusColumns $rawHeader 
            #copy (not reference) the boolColumns data for modification later.
            $boolColCopy = $boolColumns.clone() 
            
            [System.Collections.ArrayList]$rowData = @()
            #iterate over the lines in $rawContent to extract data.
            #$line=1 to skip header row.
            Write-Debug 'Start file lines.'
            for ($line = 1; $line -lt $rawContent.Length; $line++) {
                if($ShowProgress) {Show-ProgressBar -parId 2 -activityString "Parsing file: $textFile" -count $line -totalCount $rawContent.length}
                #get first date from line.
                $dataDate = $rawContent[$line].split($delimGlobal)[0]
                #average all time values from line and add to date from above.
                $dataTime = Get-AverageTimes (Get-FieldData $rawContent[$line] $timeStartCol $delimGlobal) $dataDate
                #get all data values from line.
                $dataValues = Get-FieldData $rawContent[$line] $dataStartCol $delimGlobal
                Write-Debug 'Last Get-FiledData call.'
                $keys = $boolColumns.Keys
                #Copy the changed hashtable (after first line) to original hastable. This keeps
                #track of changes to the boolean values from line to line. The $rawContent data
                #only records when a boolean changes state from True/False; if there is no state
                #change, the data field is blank. This is used to fill in those empty boolean values
                #with the correct current state.
                $boolColumns = $boolColCopy.clone()
                Write-Debug '+Start key iteration.'
                foreach($key in $Keys) {
                    if($dataValues[$key] -eq '') {
                        $dataValues[$key] = $boolColumns.$key
                    } else {
                        #use a copy of the boolean column hash to change key values (can't
                        #change the hashtable while iterating over it).
                        $boolColCopy.$key = $dataValues[$key]
                    }
                }
                #create new data row array
                $rowData.Add(( 	'(' + $SQLendQuote + `
                                ($dataTime.ToString('yyyy-MM-dd HH:mm:ss'),`
                                $dataTime.ToString('yyyy-MM-dd'),`
                                $dataTime.ToString('HH:mm:ss'),`
                                ($dataValues -join $SQLquote) -join $SQLquote) `
                                + $SQLendQuote + ')'
                            )) | Out-Null 
            }
            $sqlInsert = ('INSERT INTO `{0}`{1} VALUES {2}' -f $TableName,`
                        (Format-MySQLTableString $sqlHeader '`'),($rowData -join ","))
            if (-not $Test) {$ds = Invoke-MySQLQuery  $dbconn $sqlInsert}
            Write-Log "Processed $textFile."
            Write-verbose ("File {0,3} query string is {1} characters long ({2:n2} Mb), completed at {3:n2} seconds." -f`
                            $fCount.ToString().PadLeft(3,'0'), $sqlInsert.length,`
                            ([System.Text.Encoding]::ASCII.GetByteCount($sqlInsert) /1mb),`
                            $stopWatch.Elapsed.TotalSeconds)
        }
        if($Error) {
            Write-warning "Parse error in $textFile"
            Write-Warning $Error
            Write-Log "Parse error in $textFile"
            Write-Log $Error
            $Error.Clear()
        }
	}
    if($skipped){
        Write-Log "Processed $textFile."
        Write-verbose ("File {0,3} skipped, completed at {1:n2} seconds." -f`
                       $fCount.ToString().PadLeft(3,'0'), $stopWatch.Elapsed.TotalSeconds)
    }
}

Disconnect-MySQL $dbconn 
$stopWatch.Stop()
write-verbose ('Elapsed time: {0:n2} minutes ({1:n1} seconds).' -f`
               $stopWatch.Elapsed.TotalMinutes,$stopWatch.Elapsed.TotalSeconds)

