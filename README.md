# README #

### What is this repository for? ###

* This repo contains script(s) for converting data from Excel to txt/csv and uploading the data to a 
  MySQL/MariaDB server.

### How do I get set up? ###

* Powershell host (v3.0 or greater) required to run script(s).
* Excel must be installed on script host.
* MySQL/MariaDB .Net Connector must be installed on script host.

### Who do I talk to? ###

* Initial code by Scott McKinney (scott.mckinney@veolia.com)